/*
  @name = index.js
  @author = Ivan Lopez Arjona
  @version = 1.0
  @description = this script is used to generate boxes and calculate some operations
  @date = 2017-09-23
*/


// Comments

/*

  More

  Than

  One

  Line

*/

//JQuery code ($)
$(document).ready(function (){
  //Variables declaration
  //Strings
  var text = "Hello World";

  //Numbers
  var number1 = 12;
  var number2 = 12.25;

  //Boolean
  var correctDNI = true;
  var correctClient = false;

  //Array
  var clients = [1,2,3,4,6];

  //Variable without content
  var currentAccount;
  var current_Account;
  var currentaccount; //This is a wrong declaration

  currentAccount = 121212312; //Variable is a number
  currentAccount = "121212312"; //Now the variable is a String

  // "+" symbol very important in JavaScript
  var number3 = number2 + number1; //Both numbers are added as a result
  var text1 = "I am Peter";
  var wholeText = text + text1; // wholeText = "Hello WorldI am Peter"
  var wholeText2 = text + ", " + text1; // wholeText2 = "Hello World, I am Peter"
  var text2 = wholeText2 + number1; // text2 = "Hello World, I am Peter12"
  text2 += "Another text"; // text2 = "Hello World, I am Peter12Another text"

  //Select elements from the html document
  //$("#idName") //You get an element with id="idName"
  //$(".className") // You get all elements with class="className"
  //$("p") //You get all p's in the document
  $("img").hide();
  $("b").hide();
  $("div").hide(); //You get all div's in the document

  //$("img, b, div").hide();

  $("img").show();

    var divContent = "<form><b>Tell me a number:</b></br>";
    divContent += "<input id='boxesNumber'></input></br>";
    //divContent += "<button type='button'>Create Boxes</button></form>";

    /*var divContent = "<form><b>Tell me a number:</b></br>"+
                       "<input id='boxesNumber'></input></br>"+
                       "<button type='button'>Create Boxes</button></form>";*/

   //html() creates html content in a tag
   //append() adds html content in a tag
   $("#content").html(divContent)
                .append("<button id='generateBoxes' type='button'>Create Boxes</button></form>")

  $("div").show();

   $("#generateBoxes").click(function () {
     generateBoxes();
   });

  //alert(text);
}); //JQuery code ends here

//Angular code (ng-)


//Own code

/*
  @name = generateBoxes()
  @author = Ivan Lopez Arjona
  @version = 1.0
  @description = this function is used to...
  @date = 2017-09-23
  @params = none
  @return = none
*/
function generateBoxes (){
  $("#calculator").empty(); // Eliminates html code in a tag
  $("#calculator").show();
  var boxNumber =  $("#boxesNumber").val(); // .val -> to get value attribute in tag
  if (boxNumber>0) {
    $("#content").hide();
    for (var i = 1; i <= boxNumber; i++) {
        $("#calculator").append("<b>Box number "+i+": </b>"+"<input class='form-control' name='numbers'></input><br>");
    }
    $("#calculator").append("<button id='addNumbers' type='button'>Add Numbers</button>");
    $("#calculator").append("<button  id='back' type='button'>Back</button>");

    $("#back").click(function () {
      $("#content").show();
      $("#calculator").hide();
    });

    $("#addNumbers").click(function () {
      $("#result").empty();
      $("#result").show();
     var numbersSum = 0;
     var correctData = true;
     //to get all elements by name
     //$("[name=numbers]").each(function () {

     //to get all elements by class
     $(".form-control").each(function () {
       var thisNumber = $(this).val();
       if (isNaN(thisNumber)){ //isNaN() is a function that returns true if parameter is not a number or false if is a number
         correctData = false;
       }
       else {
           numbersSum += parseFloat(thisNumber); // to convert the content of a variable into a other type
         };

     });
     if (correctData){
       $("#calculator").hide();
       $("#result").append("The result is: "+numbersSum+"<button  id='backCalculator' type='button'>Back</button>");

       $("#backCalculator").click(function () {
         $("#result").hide();
         $("#calculator").show();
     });
   }
   else {
     alert("Some inputs are not a number");
   };

  });


  }

  else{
    alert("Wrong number");
  }
}
